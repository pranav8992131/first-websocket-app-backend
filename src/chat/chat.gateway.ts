import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';

@WebSocketGateway(8001, {
  cors: '*',
})
export class ChatGateway {
  @WebSocketServer()
  server;

  @SubscribeMessage('message')
  handleMessage(@MessageBody() message: string): void {
    console.log(message);
    this.server.emit('message', message);
  }

  // @SubscribeMessage('getUser')
  // handleUser(@MessageBody() getUser: string): void {
  //   console.log(getUser);
  //   this.server.emit('getUser', getUser);
  // }
}
